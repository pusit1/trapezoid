#include <stdio.h>

double f(double num)
{
  return num * num;
}

void main(int argc, char **argv)
{

  double a;
  double b;
  double h;
  double approx;
  double x_i;

  int n;

  scanf("%lf%lf", &a, &b);
  scanf("%d", &n);

  h = (b - a) / n;
  approx = (f(a) + f(b)) / 2.0;
  for (int i = 0; i <= n - 1; i++)
  {
    x_i = a + i * h;
    approx += f(x_i);
  }
  approx = h * approx;

  printf("answe %lf\n", approx);
}
